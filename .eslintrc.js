module.exports = {
  env: {
    browser: true,
    es6: true
  },
  overrides: [
    {
      files: ['*.test.js'],
      env: {
        mocha: true
      }
    }
  ],
  extends: [
    'standard'
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  rules: {
    'no-unused-vars': 0,
    'no-unused-expressions': 0
  }
}

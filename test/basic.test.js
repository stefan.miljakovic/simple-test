import * as operations from '../index'
import { expect } from 'chai'

const randomNumber = () => Math.round(Math.random() * 100)

const [a, b] = [randomNumber(), randomNumber()]

describe('Basic Mathematical Operations', () => {
  describe('Addition', () => {
    it('Adds 2 numbers', () => {
      expect(operations.add(a, b)).to.equal(a + b)
    })
  })
  describe('Subtraction', () => {
    it('Subtracts 2 numbers', () => {
      expect(operations.subtract(a, b)).to.equal(a - b)
    })
  })
  describe('Multiplication', () => {
    it('Multiplies 2 numbers', () => {
      expect(operations.multiply(a, b)).to.equal(a * b)
    })
  })
  describe('Division', () => {
    it('Divides 2 numbers', () => {
      expect(operations.divide(a, b)).to.equal(a / b)
    })
  })
})
